# -*- coding: utf-8 -*-
#Scanner should match:
# start of the game
# end   of the game
# kill
#players
require 'support'
require_relative '../lib/scanner'

class TestScanner < Minitest::Test

  def setup
    @parser = Quake::Scanner.new
    @line_kill = "21:07 Kill: 1022 2 22: <world> killed Isgalamido by MOD_TRIGGER_HURT"
  end

  def test_has_a_kill?
    assert  @parser.has_a_kill?(@line_kill), "Failed to match a kill"
  end

  def test_match_killer
    assert_equal "<world>", @parser.match_a_killer(@line_kill)
    assert_equal "Isgalamido", @parser.match_a_killer("22:40 Kill: 2 2 7: Isgalamido killed Isgalamido by MOD_ROCKET_SPLASH")
  end

  def test_match_killed
    assert_equal "Isgalamido", @parser.match_a_killed(@line_kill)
    assert_equal "Dono da Bola", @parser.match_a_killed("2:22 Kill: 3 2 10: Isgalamido killed Dono da Bola by MOD_RAILGUN")
  end

  def test_match_a_join_game?
    line = "21:15 ClientUserinfoChanged: 2 n\Isgalamido\t\0\model\\"
    assert @parser.match_a_join_game?(line)
  end

  def test_match_a_joining_player
    line = "21:15 ClientUserinfoChanged: 2 n\\Dono da bola\\t\\0\\model\\uriel\/zae"
    assert_equal "Dono da bola", @parser.match_a_joining_player(line)
    assert_equal "Assasinu Credi", @parser.match_a_joining_player("3:47 ClientUserinfoChanged: 5 n\\Assasinu Credi\\t\\0\\model\\sarge\\hmodel\\sarge\\g_redteam\\g_blueteam\\")
  end

  def test_match_new_game
    line = "12:13 InitGame: \\sv_floodProtect\1\\sv_maxPing\\0\\sv_minPing\\0\\sv_maxRate\\10000\\sv_minRate\\"
    assert @parser.match_new_game(line)
  end

  def test_a_mean_kill
    line = "10:24 Kill: 3 5 7: Isgalamido killed Assasinu Credi by MOD_ROCKET_SPLASH"
    assert_equal "MOD_ROCKET_SPLASH", @parser.match_a_mean_kill(line)
    assert_equal "MOD_MACHINEGUN", @parser.match_a_mean_kill("10:29 Kill: 3 4 3: Isgalamido killed Zeh by MOD_MACHINEGUN")
  end
end
