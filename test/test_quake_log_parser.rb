require 'support'
require_relative '../lib/quake_log_parser'

class TestQuakeLogParser < Minitest::Test
  def setup
    @file = File.dirname(__FILE__)+ "/../resources/log.txt"
    print @file
  end
  def test_raise_error_with_invalid_file
    assert_raises Exception do
      Quake::QuakeLogParser.new("/tmp/I_dont_exist_in_this_world")
    end
  end

 def test_with_a_valid_file
   @quake_parser = Quake::QuakeLogParser.new(@file)
   assert @quake_parser
 end

end
