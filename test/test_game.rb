require 'support'
require_relative '../lib/game'

class TestQuakeLogParser < Minitest::Test

  def test_zero_kills_on_start
    assert_equal 0, Quake::Game.new.total_kills
  end

  def test_add_player
    game  = Quake::Game.new
    player = "Rafael"
    game.add_player(player)
    assert game.players.include?(player)
  end

  def test_add_admin_player
    game  = Quake::Game.new
    player = "<world>"
    game.add_player(player)
    assert_empty game.players
  end

  def test_add_player_once
    game  = Quake::Game.new
    player = "Rafael"
    game.add_player(player)
    game.add_player(player)

    assert_equal 1, game.players.length
  end

  def test_add_kill
    game  = Quake::Game.new
    player = "Rafael"
    other_player = "Dono da Bola"
    game.add_player(player)
    game.add_kill(player, other_player)

    assert_equal 1, game.total_kills
    assert_equal 1, game.kills_from_player(player)
    game.add_kill(player, other_player)
    assert_equal 2, game.kills_from_player(player)
  end

  def test_add_kill_with_means
    game  = Quake::Game.new
    player = "Rafael"
    other_player = "Dono da Bola"
    game.add_player(player)
    game.add_kill(player, other_player, "MOD_BFG")
    assert_equal 1, game.kills_by_means.length
    assert_equal 1, game.kills_by_means["MOD_BFG"]

    game.add_kill(player, other_player, "MOD_BFG")
    assert_equal 1, game.kills_by_means.length
    assert_equal 2, game.kills_by_means["MOD_BFG"]

    game.add_kill(player, other_player, "MOD_CRUSH")
    assert_equal 2, game.kills_by_means.length
    assert_equal 1, game.kills_by_means["MOD_CRUSH"]

    game.add_kill(player, other_player)
    assert_equal 3, game.kills_by_means.length
    assert_equal 1, game.kills_by_means["MOD_UNKNOWN"]
  end

 def test_add_kill_from_world
   admin = "<world>"
   player = "Rafael"
   other_player = "Dono da Bola"

   game = Quake::Game.new

   game.add_player(player)
   game.add_player(other_player)
   game.add_player(admin)

   game.add_kill(player, other_player)
   game.add_kill(admin,player)

   assert_equal 0, game.kills_from_player(player)
   assert_equal 2, game.total_kills
   assert_equal 0, game.kills_from_player(player)
 end
end
