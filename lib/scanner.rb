module Quake
  class Scanner
    KILL = "Kill: "
    JOINED = "ClientUserinfoChanged"
    NEW_GAME = "InitGame"

    def has_a_kill?(line)
       line =~ /#{KILL}/
    end

    def match_a_join_game?(line)
      line =~ /#{JOINED}/
    end

   def match_a_killer(line)
     #any word before killed
     pattern = /[0-9 ]+: (.+) killed/
     pattern.match(line)[1]
   end

   def match_a_killed(line)
     pattern = /killed (.+[^by]) /
     pattern.match(line)[1]
   end

   def match_a_joining_player(line)
     pattern = /n\\(.+)\\t\\./
     pattern.match(line)[1]
   end

   def match_new_game(line)
     line =~ /#{NEW_GAME}/
   end

   def match_a_mean_kill(line)
     pattern = /killed (.+[^by]) by (.+[^by])/
     pattern.match(line)[2]
   end

  end
end
