module Quake
  class Player
    attr_reader :kills
    attr_reader :name
    def initialize(name)
      @name = name
      @kills = 0
    end

    def add_kill
      @kills += 1
    end

    def subtract_kill
      @kills -= 1
    end
  end
end
