# -*- coding: utf-8 -*-
require_relative 'player'
module Quake
  class Game
    attr_reader :kills
    attr_reader :total_kills
    attr_reader :players
    attr_reader :kills_by_means

    def initialize()
      @total_kills = 0
      @players = {}
      @kills = []
      @kills_by_means = {}
    end

    def add_player(player_name)
      return if is_game_admin? player_name
       unless @players.has_key?(player_name)
         player = Quake::Player.new(player_name)
         @players.merge!({player_name => player })
       end
    end

    def add_kill(killer,killed,means="MOD_UNKNOWN")
      @total_kills += 1
      add_kills_by_means(means)

      if is_game_admin? killer
        subtract_kill_to_player(killed)
      else
        add_kill_to_player(killer)
      end
    end

    def kills_from_player(player_name)
      @players[player_name].kills
    end

    def to_hash
      #example:
      #game_1: {
      #  total_kills: 45;
      #  players: ["Dono da bola", "Isgalamido", "Zeh"]
      #  kills: {
      #    "Dono da bola": 5,
      #    "Isgalamido": 18,
      #    "Zeh": 20
      # }
      #}
      list_players = []
      list_kills = {}
      @players.each_value do |p|
        list_players.push(p.name)
        list_kills.merge!({p.name => p.kills})
      end
       { total_kills: @total_kills, players: list_players, kills: list_kills }
    end

    private
    def increment_kill_player(player_name)
       @players[player_name].add_kill
    end

    def is_game_admin?(player_name)
      player_name == "<world>"
    end

    def get_player(player_name)
      @players[player_name]
    end

    def add_kill_to_player(player_name)
      increment_kill_player(player_name) if @players.has_key?(player_name)
    end

   def subtract_kill_to_player(player_name)
     get_player(player_name).subtract_kill
   end

   def add_kills_by_means(means)
     if @kills_by_means.has_key?(means)
       @kills_by_means[means] = @kills_by_means[means] + 1
     else
       @kills_by_means[means] =  1
     end

   end

  end
end
