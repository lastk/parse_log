# encoding: UTF-8
require_relative 'game'
require_relative 'scanner'
module Quake
  class QuakeLogParser
    def initialize(log_path)
      raise Exception, "File not found" unless File.exists?(log_path)
      @log = log_path
      @games = []
    end

    def process
      scanner = Quake::Scanner.new
      File.open(@log, "r").each_line do |line|
        if scanner.match_new_game(line)
          @games.push Game.new
        end

        if @games.empty?
          next
        end

        if scanner.match_a_join_game?(line)
          name = scanner.match_a_joining_player(line)
          current_game.add_player(name)
        end

        if scanner.has_a_kill?(line)
          killer = scanner.match_a_killer(line)
          killed = scanner.match_a_killed(line)
          mean   = scanner.match_a_mean_kill(line)
          current_game.add_kill(killer,killed,mean)
        end
      end
      @games
    end

    def current_game
      @games.last
    end
  end
end
