#!/usr/bin/env ruby
# encoding: UTF-8
$:.unshift(File.dirname(__FILE__) + '/../lib')

require 'quake_log_parser'

parser = Quake::QuakeLogParser.new(File.dirname(__FILE__)+ '/../resources/log.txt')
@games = parser.process

count =1
@games.each do |game|
  print("game_#{count}" => game.kills_by_means)
  count += 1
end
